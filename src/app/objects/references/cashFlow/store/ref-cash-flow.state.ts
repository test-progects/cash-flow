import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector, createSelector } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import * as Meta from "@objects/models/object.metadata";
import { RefCashFlow } from "../models/ref-cash-flow.metadata";
import { DataService } from "@services/data.service";
import { ActionsRefCashFlow as Actions} from "./ref-cash-flow.actions";


interface IModelState {
  items: RefCashFlow.IRef[];
  cash: RefCashFlow.IRef[];
}//

@State<IModelState>({
  name: 'RefCashFlow',
  defaults: {
    items: [],
    cash: []
  }
})
@Injectable()
export class RefCashFlowState {

  constructor( private _dataService: DataService ) { }

  @Action(Actions.GetList)
  actionsGetList(ctx: StateContext<IModelState>) {

    return this._dataService.getDataList_RefCashFlow().pipe(
      tap( (list) => { 
        ctx.patchState( {items: list} );
      })
    );
  }//

  @Selector()
  static selectorGetList(state: IModelState) {
    return new RefCashFlow.List( state.items );
  }//

  @Action(Actions.GetItem)
  actionsGetItem(ctx: StateContext<IModelState>, {id}: Actions.GetItem) {
    if (!id) return;
    return this._dataService.getData_RefCashFlow(id).pipe(
      tap( (val) => { 
        const { cash } = ctx.getState();
        const newCash = cash.filter(item =>  !(item && item.id === id));
        newCash.push(val);
        ctx.patchState( {cash: newCash} );
      } )
    );
  }//

  static selectorGetItem(id: Meta.dbRef, isCopy?: boolean ) {
    return createSelector([RefCashFlowState], (state: IModelState) => {
      const rec = state.cash.find( item=>item.id===id );
      return new RefCashFlow.Ref( rec, isCopy );
    });
  }//

  @Action(Actions.NewItem)
  actionsNewItem(ctx: StateContext<IModelState>) {
  }//

  @Action(Actions.SetDeleteMark)
  actionsSetDeleteMark(ctx: StateContext<IModelState>, {id}: Actions.SetDeleteMark) {
  }//

  @Action(Actions.SaveItem)
  actionsSaveItem(ctx: StateContext<IModelState>, {obj}: Actions.SaveItem) {
    return this._dataService.save_RefCashFlow(obj);
  }//


}//

