import { Component, Input, Output, EventEmitter } from '@angular/core';
//import * as Meta from "@objects/models/object.metadata";


@Component({
  selector: 'app-doc-list-command-panel-up',
  templateUrl: './doc-list-command-panel-up.component.html',
})
export class DocListCommandPanelUpComponent {

  @Input() object: any;//ListDocCashOrder

  @Output("clickMenu") eventClickMenu = new EventEmitter<any>();

  constructor() { }

  //

  clickNew() {
    this.eventClickMenu.emit( {name:"clickNew"} );
  }//

  clickCopy() {
    this.eventClickMenu.emit( {name:"clickCopy"} );
  }//

}//
