import { NgModule } from '@angular/core';
import { SharedModule } from "@app/modules/shared.module";
//
import { DocCommandPanelUpComponent } from "./doc-command-panel-up/doc-command-panel-up.component";
import { DocCommandPanelDownComponent } from "./doc-command-panel-down/doc-command-panel-down.component";
import { DocListCommandPanelUpComponent } from "./doc-list-command-panel-up/doc-list-command-panel-up.component";

@NgModule({

  imports: [
    SharedModule
  ],

  declarations: [
    DocCommandPanelUpComponent,
    DocCommandPanelDownComponent,
    DocListCommandPanelUpComponent,
  ],

  exports: [
    DocCommandPanelUpComponent,
    DocCommandPanelDownComponent,
    DocListCommandPanelUpComponent,
  ]
})
export class DocElementsModule {}
