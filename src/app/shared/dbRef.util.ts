import { v4 as uuid4 } from "uuid";
import * as Meta from "@objects/models/object.metadata";

//simple=94df9991-b975-488b-a0b7-a709fb0e30e4

export function getNewReference(tableName) {

    if (!tableName) {return ''}

    return tableName + "=" + uuid4();
}//

export function isReference(ref: Meta.dbRef) {

    if (!ref) {return false}

    if (!ref.includes('=')) {return false}

    return true;
}//
