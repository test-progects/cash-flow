
import { formatDate, is, extractDate, fillProp } from "@shared/other.util";
import { Observable, of, EMPTY, concat } from 'rxjs';
import { toArray } from 'rxjs/operators';
import { DataService } from '@services/data.service';
import { getNewReference } from "@shared/dbRef.util";
import * as Meta from "@objects/models/object.metadata";

export namespace DocCashOrder {

    export interface IDoc {
        id: Meta.dbRef;
        deleteMark: boolean;
        doneMark: boolean;
        //
        number: number;
        date?: Date | string;
        //
        typeMoverment: Meta.EInOut;
        sum: number;
        cashFlowItem: Meta.dbRef;
        comment: string;
    }//

    export interface IDocHttp {
        list: IDoc[];
    }//

    export class Doc extends Meta.MetadataObject implements IDoc  {

        id:                     Meta.dbRef  = null;
        deleteMark:             boolean     = null;
        doneMark:               boolean     = null;
        number:                 number      = null;
        private _date:          Date;
        typeMoverment:          Meta.EInOut = null;
        typeMoverment_title_:   string      = null;
        sum:                    number      = null;
        cashFlowItem:           Meta.dbRef  = null;
        cashFlowItem_title_:    string      = null;
        comment:                string      = null;

        get title() : string {
            return "" + this.number;
        }//
        
        set date(v : string | Date) {
            let d = extractDate(v);
            if (d) { this._date = d }
        }//

        get date() { return this._date; }

        constructor ( record?: IDoc, isCopy?: boolean ) {
            super();
            if (!record) {
                //Это новый объект
                this.id = getNewReference('DocCashOrder');
                this.date = new Date;
                return;
            };

            fillProp(this, record);
            this.date = record.date;

            if (isCopy) this.id = getNewReference('DocCashOrder');
        }//

        getRecordDB () {
            return {
                id:             this.id,
                deleteMark:     this.deleteMark,
                doneMark:       this.doneMark,
                //
                number:         this.number,
                date:           this.date,
                //
                typeMoverment:  this.typeMoverment,
                sum:            this.sum,
                cashFlowItem:   this.cashFlowItem,
                comment:        this.comment
            }
        }//

        refFields() { return ["cashFlowItem"] }

        //Все дополнительные операции, чтобы полностью "развернуть" объект.
        refresh(dataServise: DataService): Observable<any> {

            let o$: Observable<any> = EMPTY;

            //Получение представления полей:

            this.typeMoverment_title_ = Meta.InOutTitle.getTitle(this.typeMoverment);

            let titleCF$ = dataServise.getTitle(this.cashFlowItem);
            titleCF$.subscribe(val=>{ this.cashFlowItem_title_ = val });
            o$ = concat(o$, titleCF$);

            return o$;//Флаг завершения операции.
        }//
        
    }//

    export class RowList {

        id:                     Meta.dbRef  = null;
        deleteMark:             boolean     = null;
        doneMark:               boolean     = null;
        number:                 number      = null;
        private _date:          Date;
        typeMoverment:          Meta.EInOut = null;
        typeMoverment_title_:   string      = null;
        sum:                    number      = null;
        cashFlowItem:           Meta.dbRef  = null;
        cashFlowItem_title_:    string      = null;
        comment:                string      = null;

        refFields() { return ["cashFlowItem"] }

        constructor ( record?: IDoc ) {
            if (!record) return;

            fillProp(this, record);
            this.date = record.date;
        }//

        set date(v : string | Date) {
            let d = extractDate(v);
            if (d) { this._date = d }
        }//

        get date() { return this._date; }

        //Все дополнительные операции, чтобы полностью "развернуть" объект.
        refresh(dataServise: DataService): Observable<any> {

            let o$: Observable<any> = EMPTY;

            //Получение представления полей:

            this.typeMoverment_title_ = Meta.InOutTitle.getTitle(this.typeMoverment);

            let titleCF$ = dataServise.getTitle(this.cashFlowItem);
            titleCF$.subscribe(val=>{ this.cashFlowItem_title_ = val });
            o$ = concat(o$, titleCF$);

            return o$;//Флаг завершения операции.
        }//

    }//

    export class List {
    
        public list: RowList[] = [];

        constructor ( recordList?: IDoc[] ) {
            if (!recordList) return;
            for (let rec of recordList) { this.list.push( new RowList(rec) ) }
        }//

        refresh(dataServise: DataService):Observable<any> {

            let o$: Observable<any> = EMPTY;
            this.list.forEach(el => { 
                o$ = concat(o$, el.refresh(dataServise));
            });
            return o$.pipe(toArray());
        }//

    }//

}//DocCashOrder

