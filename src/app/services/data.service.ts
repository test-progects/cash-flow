import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { tap, delayWhen, map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

import * as Meta from "@objects/models/object.metadata";
import { DocCashOrder } from "@objects/documents/cashOrder/models/doc-cash-order.metadata";
import { RefCashFlow } from "@objects/references/cashFlow/models/ref-cash-flow.metadata";

import { isReference } from "@shared/dbRef.util";
import { formatDate } from "@shared/other.util";


@Injectable()
export class DataService {
  
  refUrl = `${environment.apiUrl}refCashFlow.json`;
  docUrl = `${environment.apiUrl}docCashOrder.json`;

  //Это "база данных", т.е. хранение в массивах в простом виде, как пришло с "бэка".
  private _dataDocCashOrder: DocCashOrder.IDoc[] = [];
  private _dataRefCashFlow: RefCashFlow.IRef[] = [];

  private _onLoad$: Observable<any>;

  constructor(private _http: HttpClient) { 
    this.initialiseData();
  }//

  initialiseData() {
    
    let ref$ = this._http.get<RefCashFlow.IRefHttp>(this.refUrl);
    ref$.subscribe((obj) => { 
      this._dataRefCashFlow = obj.list;
    });
  
    let doc$ = this._http.get<DocCashOrder.IDocHttp>(this.docUrl);
    doc$.subscribe((obj) => { 
      this._dataDocCashOrder = obj.list;
    });

    this._onLoad$ = doc$;
  }//

  getDataList_RefCashFlow(): Observable<RefCashFlow.IRef[]> {

    return of(0).pipe(
      delayWhen(_=>this._onLoad$),
      map(_=>this._dataRefCashFlow)
      );
  }// 

  getDataList_DocCashOrder(): Observable<DocCashOrder.IDoc[]> {
    
    return of(0).pipe(
      delayWhen(_=>this._onLoad$),
      map(_=>this._dataDocCashOrder),
      );
  }// 

  getData_DocCashOrder(id:Meta.dbRef): Observable<DocCashOrder.IDoc> {

    return of(0).pipe(
      delayWhen(_=>this._onLoad$),
      map(_=>{
        const doc = this._dataDocCashOrder.find(item=>item.id === id);
        return (doc) ? doc : undefined; 
      })
    );
 
  }// 

  getData_RefCashFlow(id:Meta.dbRef): Observable<RefCashFlow.IRef> {

    return of(0).pipe(
      delayWhen(_=>this._onLoad$),
      map(_=>{
        let item = this._dataRefCashFlow.find(item=>item.id === id);
        return (item) ? item : undefined; 
      })
    );
  }// 

  save_DocCashOrder(obj:DocCashOrder.IDoc): Observable<boolean> {
    if (!obj.id) return of(false);

    const m = this._dataDocCashOrder.filter( item=>item.id !== obj.id );
    m.push(obj);
    this._dataDocCashOrder = m;
    return of(true);
  }//

  save_RefCashFlow(obj:RefCashFlow.IRef): Observable<boolean> {
    if (!obj.id) return of(false);

    const m = this._dataRefCashFlow.filter( item=>item.id !== obj.id );
    m.push(obj);
    this._dataRefCashFlow = m;
    return of(true);
  }//

  findByRef(ref: Meta.dbRef) {

    if (!isReference(ref)) { return undefined }

    let lengthTableName = ref.indexOf('=');
    let tableName = ref.substr(0, lengthTableName);

    let db = [];
    switch (tableName) {
      case "docCashOrder":
        db = this._dataDocCashOrder;
        break;
    
      case "refCashFlow":
        db = this._dataRefCashFlow;
        break;
    }

    let item = undefined;
    for (let i of db) {
      if (i.id == ref) { item = Object.assign({}, i); break; }
    }

    if (!item) { return undefined }

    switch (tableName) {
      case "docCashOrder": 
        item._title = "Cash order №" + item.number + " of " + formatDate(item.date);
        break;

      case "refCashFlow": 
        item._title = item.name;
        break;
    }

    return item;
  }//


  getTitle(ref: Meta.dbRef): Observable<string> {

    let item = this.findByRef(ref);     
    
    if (item) { return of(item._title) }

    return of("<>");
  }//

}//

