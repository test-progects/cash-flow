import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DocCashOrder } from "../../models/doc-cash-order.metadata";
import { styleKit, getDocIcon, getDocIconStyle } from "@shared/other.util";


@Component({
  selector: 'app-list-doc-cash-order',
  templateUrl: './list-doc-cash-order.component.html',
  styleUrls: ['./list-doc-cash-order.component.scss']
})
export class ListDocCashOrderComponent implements OnInit {

  @Input() list: DocCashOrder.RowList[];
  displayedColumns: string[] = ['icon', 'date', 'number', 'typeMoverment', 'sum', 'cashFlowItem', 'comment'];

  public currentRow: DocCashOrder.RowList;
  @Output() currentRowChange = new EventEmitter<DocCashOrder.RowList>();
  @Output() selectRow = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }//

  keyDown(event){
    const key = event.key;

    switch (key) {
      case "ArrowUp": this.rowUp();
        break;
      case "ArrowDown": this.rowDown();
        break;
      case "Enter": this.selectRow.emit();
        break;  
    
      default: break;
    }
    
  }//


  rowDown(){
    event.preventDefault();
    if (this.list.length == 0) return;
    if (!this.currentRow) this.currentRow = this.list[0];
    let i = this.list.indexOf(this.currentRow);
    if (i < this.list.length - 1) this.currentRow = this.list[i+1];
    this.currentRowChange.emit(this.currentRow);
  }//

  rowUp() {
    event.preventDefault();
    if (this.list.length == 0) return;
    if (!this.currentRow) this.currentRow = this.list[0];
    let i = this.list.indexOf(this.currentRow);
    if (i > 0) this.currentRow = this.list[i-1];
    this.currentRowChange.emit(this.currentRow);
  }//


  rowClick(row){
    this.currentRow = row;
    this.currentRowChange.emit(this.currentRow);
  }//

  rowDoubleClick(row){
    this.selectRow.emit();
  }//

  getRowStyle(row) {
    return (row === this.currentRow) ? styleKit.currentRow : "";
  }//

  getIcon(row: DocCashOrder.RowList) {
    return getDocIcon(row);
  }//

  getIconStyle(row: DocCashOrder.RowList) {
    return getDocIconStyle(row);
  }//


}//
