import { NgModule } from '@angular/core';
import { SharedModule } from "@app/modules/shared.module";
//
import { RefElementsModule } from "@objects/references/elements/ref-elements.module";
import { ListRefCashFlowContainer } from "./list/list-ref-cash-flow.container";
import { ListRefCashFlowComponent } from "./list/component/list-ref-cash-flow.component";
import { SelectRefCashFlowContainer } from "./listForSelect/select-ref-cash-flow.container";
import { RefCashFlowContainer } from "./ref/ref-cash-flow.container";
import { RefCashFlowComponent } from "./ref/component/ref-cash-flow.component";
import { RefCashFlowRoutingModule } from "./ref-cash-flow.routing";

@NgModule({


  imports: [
    SharedModule,
    RefElementsModule,
    RefCashFlowRoutingModule,
  ],

  declarations: [
    ListRefCashFlowContainer,
    ListRefCashFlowComponent,
    SelectRefCashFlowContainer,
    RefCashFlowContainer,
    RefCashFlowComponent
  ],

  exports: [
    RefCashFlowRoutingModule,
    //
    ListRefCashFlowContainer,
    ListRefCashFlowComponent,
    SelectRefCashFlowContainer,
    RefCashFlowContainer,
    RefCashFlowComponent
  ],

  entryComponents: [
    SelectRefCashFlowContainer
  ]

})
export class RefCashFlowModule {}
