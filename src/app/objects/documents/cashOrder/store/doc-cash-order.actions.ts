import * as Meta from "@objects/models/object.metadata";
import { DocCashOrder } from "../models/doc-cash-order.metadata";


export namespace ActionsDocCashOrder {

    export class GetList {
        static readonly type = '[DocCashOrder] GetList';
        constructor() {}
    }//

    export class GetDoc {
        static readonly type = '[DocCashOrder] GetDoc';
        constructor(public id: Meta.dbRef) {}
    }//

    export class NewDoc {
        static readonly type = '[DocCashOrder] NewDoc';
        constructor() {}
    }//

    export class SaveDoc {
        static readonly type = '[DocCashOrder] SaveDoc';
        constructor(public obj: DocCashOrder.IDoc) {}
    }//

    export class SetDeleteMark {
        static readonly type = '[DocCashOrder] SetDeleteMark';
        constructor(public id: Meta.dbRef) {}
    }//
        
}//

