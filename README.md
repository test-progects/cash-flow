# CashFlow

Пробный проект на Angular.
Учетная система "Движение денежных средств". Один документ (форма и список), один справочник.

## Отрабатывались технологии:

Принцип "умного и глупого" компонентов. 
В "умном" компоненте (container) реализованы механизмы загрузки и подготовки данных, а также вся бизнес-логика отбработки событий формы документа. В "глупом" (component), реализована только форма; события, требующие обработки, передаются наверх, в "умный" компонент.  

У "умного" и "глупого" компонентов есть общий реквизит - основной объект. Это инстанс класса (справочника, документа), реализующий все методы работы с данными, которые могут понадобиться компонентам для его отображения или бизнес-логики. Например, в нем есть метод refresh(), обновляющий представления всех ссылочных полей объекта.

Остальное: NGXS, Angular Material.

## Структура проекта

    objects - здесь описаны отдельные объекты метаданных (справочники, документы), из которых состоит проект.
        \documents
            \cashOrder - объект "документ - кассовый ордер"
                \doc - карточка документа, "умный" компонент
                    \component - "глупый" компонент
                \list - форма списка документов
                \models - описание класса документа
                \store - state этого типа данных
        \elements - отдельные компоненты, для использования в любых документах        
        doc-cash-order.module.ts - модуль для подключения этого документа в проект






## ...

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.21.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
