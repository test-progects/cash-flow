import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { delayWhen } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { Router, ActivatedRoute } from '@angular/router';
//
import * as Meta from "@objects/models/object.metadata";
import { RefCashFlow } from "../models/ref-cash-flow.metadata";
import { DataService } from "@services/data.service";
import { ActionsRefCashFlow as Actions } from "../store/ref-cash-flow.actions";
import { RefCashFlowState as State } from '../store/ref-cash-flow.state';
//
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  templateUrl: './select-ref-cash-flow.container.html',
  styleUrls: ['./select-ref-cash-flow.container.scss']
})
export class SelectRefCashFlowContainer implements OnInit {

  public object$: Observable<RefCashFlow.List>;
  public currentRow: RefCashFlow.Ref;
  public withChildren: boolean;

  constructor( 
    private store: Store, 
    private _dataService: DataService,  
    private router: Router,
    private route: ActivatedRoute,
    public dialogRef: MatDialogRef<SelectRefCashFlowContainer>,
    @Inject(MAT_DIALOG_DATA) public options: Meta.IOptionsDialog 
    ) { }//

  ngOnInit() {

    this.store.dispatch(new Actions.GetList());

    this.object$ = this.store.select(State.selectorGetList)
    .pipe(
      delayWhen(val=>{return val.refresh(this._dataService)})
    );

  }//

  currentRowChange(val) {
    if (!val) return;
    this.currentRow = val;
  }//

  clickMenuUp(event) {
    switch (event.name) {

      case "clickSelect":
        this.selectRow();
        break;
        
      default: break;
    }
  }//

  selectRow() {
    if (!this.currentRow) return;
    this.dialogRef.close(this.currentRow);
  }//

  newCopy() {
  }//


}//
