import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as Meta from "@objects/models/object.metadata";

@Component({
  selector: 'app-doc-command-panel-down',
  templateUrl: './doc-command-panel-down.component.html',
})
export class DocCommandPanelDownComponent {

  @Input() object: Meta.MetadataObject;

  @Output("clickMenu") eventClickMenu = new EventEmitter<any>();

  constructor() { }

  //

  clickOK() {
    this.eventClickMenu.emit( {name:"clickOK"} );
  }//

  clickExit() {
    this.eventClickMenu.emit( {name:"clickExit"} );
  }//

}//

