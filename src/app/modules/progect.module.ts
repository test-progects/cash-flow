import { NgModule } from '@angular/core';
import { SharedModule } from "./shared.module";
import { DocCashOrderModule } from "@objects/documents/cashOrder/doc-cash-order.module";
import { RefCashFlowModule } from "@objects/references/cashFlow/ref-cash-flow.module";
import { DocCashOrderState } from "@objects/documents/cashOrder/store/doc-cash-order.state";
import { RefCashFlowState } from "@objects/references/cashFlow/store/ref-cash-flow.state";
import { environment } from "@app/../environments/environment";
import { NgxsModule } from '@ngxs/store';


@NgModule({

  imports: [
    SharedModule,
    DocCashOrderModule,
    RefCashFlowModule,
    NgxsModule.forRoot([DocCashOrderState, RefCashFlowState], { developmentMode: !environment.production }),
  ],

  declarations: [],

  exports: []
})
export class ProgectModule {}
