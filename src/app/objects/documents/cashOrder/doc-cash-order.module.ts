import { NgModule } from '@angular/core';
import { SharedModule } from "@app/modules/shared.module";
//
import { DocElementsModule } from "@objects/documents/elements/doc-elements.module";
import { DocCashOrderContainer } from "./doc/doc-cash-order.container";
import { DocCashOrderComponent } from "./doc/component/doc-cash-order.component";
import { ListDocCashOrderContainer } from "./list/list-doc-cash-order.container";
import { ListDocCashOrderComponent } from "./list/component/list-doc-cash-order.component";
import { DocCashOrderRoutingModule } from "./doc-cash-order.routing";

@NgModule({

  imports: [
    SharedModule,
    DocElementsModule,
    DocCashOrderRoutingModule,
  ],

  declarations: [
    DocCashOrderContainer,
    DocCashOrderComponent,
    ListDocCashOrderContainer,
    ListDocCashOrderComponent
  ],

  exports: [
    DocCashOrderRoutingModule,
    //
    DocCashOrderContainer,
    DocCashOrderComponent,
    ListDocCashOrderContainer,
    ListDocCashOrderComponent
  ]

})
export class DocCashOrderModule {}
