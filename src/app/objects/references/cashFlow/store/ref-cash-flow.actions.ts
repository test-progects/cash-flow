import * as Meta from "@objects/models/object.metadata";
import { RefCashFlow } from "../models/ref-cash-flow.metadata";

export namespace ActionsRefCashFlow {

    export class GetList {
        static readonly type = '[RefCashFlow] GetList';
        constructor() {}
    }//

    export class GetItem {
        static readonly type = '[RefCashFlow] GetItem';
        constructor(public id: Meta.dbRef) {}
    }//

    export class NewItem {
        static readonly type = '[RefCashFlow] NewItem';
        constructor() {}
    }//

    export class SaveItem {
        static readonly type = '[RefCashFlow] SaveItem';
        constructor(public obj: RefCashFlow.IRef) {}
    }//


    export class SetDeleteMark {
        static readonly type = '[RefCashFlow] SetDeleteMark';
        constructor(public id: Meta.dbRef) {}
    }//
        
}//