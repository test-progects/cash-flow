import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { delayWhen } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { Router, ActivatedRoute } from '@angular/router';

import { RefCashFlow } from "../models/ref-cash-flow.metadata";
import { DataService } from "@services/data.service";
import { ActionsRefCashFlow as Actions } from "../store/ref-cash-flow.actions";
import { RefCashFlowState as State } from '../store/ref-cash-flow.state';


@Component({
  templateUrl: './list-ref-cash-flow.container.html',
  styleUrls: ['./list-ref-cash-flow.container.scss']
})
export class ListRefCashFlowContainer implements OnInit {

  public object$: Observable<RefCashFlow.List>;
  public currentRow: RefCashFlow.Ref;
  public withChildren: boolean;

  constructor( 
    private store: Store, 
    private _dataService: DataService,  
    private router: Router,
    private route: ActivatedRoute,
    ) {

      route.data.subscribe(data=>{ 
        this.withChildren = !!data.withChildren });
    }//

  ngOnInit() {

    this.store.dispatch(new Actions.GetList());

    this.object$ = this.store.select(State.selectorGetList)
    .pipe(
      delayWhen(val=>{return val.refresh(this._dataService)})
    );

  }//

  currentRowChange(val) {
    if (!val) return;
    this.currentRow = val;
  }//

  clickMenuUp(event) {
    switch (event.name) {
      case "clickNew":
        this.router.navigate(['/ListRefCashFlow', 'RefCashFlow', ""]);
        break;

      case "clickCopy":
        this.newCopy();
        break;
        
      default: break;
    }
  }//

  selectRow() {
    if (!this.currentRow) return;
    this.router.navigate(['/ListRefCashFlow', 'RefCashFlow', this.currentRow.id]);
  }//

  newCopy() {
    if (!this.currentRow) return;
    this.router.navigate(['/ListRefCashFlow', 'RefCashFlow', this.currentRow.id, "copy"]);
  }//


}//
