import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as Meta from "@objects/models/object.metadata";

@Component({
  selector: 'app-doc-command-panel-up',
  templateUrl: './doc-command-panel-up.component.html',
})
export class DocCommandPanelUpComponent  {

  @Input() object: Meta.MetadataObject;

  @Output("clickMenu") eventClickMenu = new EventEmitter<any>();

  constructor() { }

  //


  clickOK() {
    this.eventClickMenu.emit( {name:"clickOK"} );
  }//

  clickSave() {
    this.eventClickMenu.emit( {name:"clickSave"} );
  }//

  clickCopy() {
    this.eventClickMenu.emit( {name:"clickCopy"} );
  }//

  clickDel() {
    this.eventClickMenu.emit( {name:"clickDel"} );
  }//

  clickGo() {
    this.eventClickMenu.emit( {name:"clickGo"} );
  }//

  clickStop() {
    this.eventClickMenu.emit( {name:"clickStop"} );
  }//

  clickViewInList() {
    this.eventClickMenu.emit( {name:"clickViewList"} );
  }//

  clickPrint() {
    this.eventClickMenu.emit( {name:"clickPrint"} );
  }//

  clickHelp() {
    this.eventClickMenu.emit( {name:"clickHelp"} );
  }//

  clickExit() {
    this.eventClickMenu.emit( {name:"clickExit"} );
  }//

}//
