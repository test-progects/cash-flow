import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { RefCashFlow } from "../../models/ref-cash-flow.metadata";

@Component({
  selector: 'app-ref-cash-flow',
  templateUrl: './ref-cash-flow.component.html',
  styleUrls: ['./ref-cash-flow.component.scss']
})
export class RefCashFlowComponent implements OnInit, OnChanges {

  @Input() object: RefCashFlow.Ref;

  @Output() eventFormChange = new EventEmitter<any>();

  form: FormGroup;

  constructor( private formBuilder: FormBuilder ) {

    this.form = formBuilder.group({
      code:           ['', [Validators.required]],
      name:           ['', [Validators.required]],
      comment:        ['', []]
    });
  }//

  ngOnChanges(change: SimpleChanges) {
    this.objectToForm();
  }//

  formToObject() {
    const v = this.form.value;
    this.object.code          = v.code;
    this.object.name          = v.name;
    this.object.comment       = v.comment;
  }//

  objectToForm() {
    this.form.patchValue(this.object);
  }//
 
  ngOnInit() {
  }//

  onChangeField(event, name) {
    this.formToObject();
    this.eventFormChange.emit({ name: name, value: this.form.value[name], event: event });
  }//

}//
