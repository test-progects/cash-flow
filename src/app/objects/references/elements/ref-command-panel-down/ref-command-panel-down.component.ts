import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MetadataObject } from "../../../models/object.metadata";


@Component({
  selector: 'app-ref-command-panel-down',
  templateUrl: './ref-command-panel-down.component.html',
})
export class RefCommandPanelDownComponent {

  @Input() object: MetadataObject;

  @Output("clickMenu") eventClickMenu = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  //

  clickOK() {
    this.eventClickMenu.emit( {name:"clickOK"} );
  }//

  clickExit() {
    this.eventClickMenu.emit( {name:"clickExit"} );
  }//

}//



