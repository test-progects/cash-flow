import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListRefCashFlowContainer } from "./list/list-ref-cash-flow.container";
import { RefCashFlowContainer } from "./ref/ref-cash-flow.container";

const routes: Routes = [
  
  { path: 'ListRefCashFlow',  
    pathMatch: 'full', 
    component: ListRefCashFlowContainer, 
    data: { withChildren: false } 
  },

  { path: 'ListRefCashFlow', 
    component: ListRefCashFlowContainer,
    children: [
    { path: 'RefCashFlow/:id',
      component: RefCashFlowContainer
    }],
    data: { withChildren: true }
  },

  { path: 'ListRefCashFlow', 
    component: ListRefCashFlowContainer,
    children: [
    { path: 'RefCashFlow/:id/:copy',
      component: RefCashFlowContainer
    }],
    data: { withChildren: true }
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class RefCashFlowRoutingModule { }
