import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { formatDate as fDate} from "@shared/other.util";
import { DateTimeAdapter } from 'ng-pick-datetime';

import * as Meta from "@objects/models/object.metadata";
import { DocCashOrder } from "../../models/doc-cash-order.metadata";


@Component({
  selector: 'app-doc-cash-order',
  templateUrl: './doc-cash-order.component.html',
  styleUrls: ['./doc-cash-order.component.scss']
})
export class DocCashOrderComponent implements OnInit, OnChanges {

  @Input() object: DocCashOrder.Doc;

  @Output() eventFormChange = new EventEmitter<any>();

  form: FormGroup;

  move = [ { value: Meta.EInOut.In, viewValue: "Приход" }, { value: Meta.EInOut.Out, viewValue: "Расход" } ];

  
  constructor( private formBuilder: FormBuilder, dateTimeAdapter: DateTimeAdapter<any> ) {

    dateTimeAdapter.setLocale('ru-RU');

    this.form = formBuilder.group({

      number:                 ['', [Validators.required]],
      date:                   ['', [Validators.required]],
      typeMoverment:          ['', [Validators.required]],
      typeMoverment_title_:   ['', []],
      sum:                    ['', [Validators.required]],
      cashFlowItem:           ['', [Validators.required]],
      cashFlowItem_title_:    ['', []],
      comment:                ['', []]
            
    });

  }//

  ngOnChanges(change: SimpleChanges) {
    this.objectToForm();
  }//

  formToObject() {
    const v = this.form.value;
    this.object.number        = v.number;
    this.object.date          = v.date;
    this.object.typeMoverment = v.typeMoverment;
    this.object.sum           = v.sum;
    this.object.cashFlowItem  = v.cashFlowItem;
    this.object.comment       = v.comment;
  }//

  objectToForm() {
    if (!this.object) return;
    this.form.patchValue(this.object);
    this.form.patchValue( {date: this.object.date } );
  }//

  startDate() { 
    if (this.object && this.object.date) return this.object.date;
    return new Date();
  }//
 
  ngOnInit() {
  }//

  clickCashFlow() {
    this.eventFormChange.emit({ name: "clickCashFlow" });
  }//
  
  onChangeField(event, name) {
    this.formToObject();
    this.eventFormChange.emit({ name: name, value: this.form.value[name], event: event });
  }//


}//
