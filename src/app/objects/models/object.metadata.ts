
import { DataService } from '@services/data.service';

export type dbRef = string; //simple=94df9991-b975-488b-a0b7-a709fb0e30e4

export enum EInOut {
    In = "In",
    Out = "Out"

}//

export const InOutTitle = {
    
    _title: { [EInOut.In]: "Приход", [EInOut.Out]: "Расход" },

    getTitle: function(item) { 
        if (!item) {return ''}
        return this._title[item]; 
    }
}//

export interface IOptionsDialog {
    select?: boolean
}//

export class MetadataObject {

    id: string;
    refresh(dataServise: DataService) {}
}//
