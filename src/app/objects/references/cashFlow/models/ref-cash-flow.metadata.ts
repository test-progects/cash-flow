
import { fillProp } from "@shared/other.util";
import { Observable, of, EMPTY, concat } from 'rxjs';
import { toArray } from 'rxjs/operators';
import { DataService } from '@services/data.service';
import { getNewReference } from "@shared/dbRef.util";
import * as Meta from "@objects/models/object.metadata";

export namespace RefCashFlow {

    export interface IRef {
        id: Meta.dbRef;
        deleteMark: boolean;
        //
        code: string;
        name: string;
        comment: string;
    }//

    export interface IRefHttp {
        list: IRef[];
    }//

    export class Ref extends Meta.MetadataObject implements IRef  {

        id:             Meta.dbRef  = null;
        deleteMark:     boolean     = null;
        code:           string      = null;
        name:           string      = null;
        comment:        string      = null;

        get title() : string {
            return this.name;
        }//
        
        constructor ( record?: IRef, isCopy?: boolean  ) {

            super();
            if (!record) {
                //Это новый объект
                this.id = getNewReference('RefCashFlow');
                return;
            };

            fillProp(this, record);
            if (isCopy) this.id = getNewReference('RefCashFlow');
        }//

        refresh(dataServise: DataService): Observable<any> {
            return of(0);
        }//

        getRecordDB () {
            return {
                id:             this.id,
                deleteMark:     this.deleteMark,
                //
                code:           this.code,
                name:           this.name,
                comment:        this.comment
            }
        }//
        
    }//

    export class List {
    
        public list: Ref[] = [];

        constructor ( recordList?: IRef[] ) {
            if (!recordList) return;
            for (let rec of recordList) { this.list.push( new Ref(rec) ) }
        }//

        refresh(dataServise: DataService):Observable<any> {

            let o$: Observable<any> = EMPTY;
            this.list.forEach(el => { 
                o$ = concat(o$, el.refresh(dataServise));
            });
            return o$.pipe(toArray());
        }//

    }//

}//RefCashFlow
