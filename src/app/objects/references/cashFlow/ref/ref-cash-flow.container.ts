import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { delayWhen } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngxs/store';
//
import * as Meta from "@objects/models/object.metadata";
import { RefCashFlow } from "../models/ref-cash-flow.metadata";
import { ActionsRefCashFlow as Actions } from "../store/ref-cash-flow.actions";
import { RefCashFlowState as State } from '../store/ref-cash-flow.state';
import { DataService } from "@services/data.service";


@Component({
  templateUrl: './ref-cash-flow.container.html',
  styleUrls: ['./ref-cash-flow.container.scss']
})
export class RefCashFlowContainer implements OnInit {

  public object$: Observable<RefCashFlow.Ref>;
  public object: RefCashFlow.Ref;
  private _id: string;
  private _isCopy: boolean;

  constructor( 
    private store: Store, 
    private _dataService: DataService,
    private route: ActivatedRoute,
    private router: Router
    ) {

      route.params.subscribe((par) => {
        this._id = par['id'];
        this._isCopy = !!par.copy;
        this.loadData();
      });
    }//

  ngOnInit() {}

  loadData() {
    const id = this._id as Meta.dbRef;
    this.store.dispatch(new Actions.GetItem(id)).toPromise().then(_=>{
      
      this.store.selectOnce(State.selectorGetItem(id, this._isCopy))
      .pipe(
        delayWhen(val=>{return val.refresh(this._dataService)})
      ).toPromise().then(val=>{ this.object = val });
    });
  }//

  
  eventFormChange(ctx) {}
    

  //

  clickMenuUp(event) {
    if (!event) return;

    switch (event.name) {

      case 'clickSave':
        this.onSave();
        break;
    
      default: break;
    }
  }//


  clickMenuDown(event) {
    if (!event) return;

    switch (event.name) {
      case 'clickExit':
        if (this.onClose()) this.router.navigate(['/ListRefCashFlow']);
        break;

      case 'clickOK':
        this.onSave();
        if (this.onClose()) this.router.navigate(['/ListRefCashFlow']);
        break;
    
      default: break;
    }

  }//

  //

  onClose() {
    return true;
  }//

  onSave() {
    const record = this.object.getRecordDB();
    this.store.dispatch(new Actions.SaveItem(record));
  }//

}//
