import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListDocCashOrderContainer } from "./list/list-doc-cash-order.container";
import { DocCashOrderContainer } from "./doc/doc-cash-order.container";


const routes: Routes = [
  
  { path: 'ListDocCashOrder',  
    pathMatch: 'full', 
    component: ListDocCashOrderContainer, 
    data: { withChildren: false } 
  },

  { path: 'ListDocCashOrder', 
    component: ListDocCashOrderContainer,
    children: [
      { path: 'DocCashOrder/:id',
        component: DocCashOrderContainer
      }],
    data: { withChildren: true }
  },

  { path: 'ListDocCashOrder', 
    component: ListDocCashOrderContainer,
    children: [
      { path: 'DocCashOrder/:id/:copy',
        component: DocCashOrderContainer
      }],
    data: { withChildren: true }
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class DocCashOrderRoutingModule { }
