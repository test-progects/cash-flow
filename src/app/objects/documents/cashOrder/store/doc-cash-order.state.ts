import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector, createSelector } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import * as Meta from "@objects/models/object.metadata";
import { DocCashOrder } from "../models/doc-cash-order.metadata";
import { DataService } from "@services/data.service";
import { ActionsDocCashOrder as Actions} from "./doc-cash-order.actions";


interface IModelState {
  items: DocCashOrder.IDoc[];
  cash: DocCashOrder.IDoc[];
}//

@State<IModelState>({
  name: 'DocCashOrder',
  defaults: {
    items: [],
    cash: []
  }
})
@Injectable()
export class DocCashOrderState {

  constructor( private _dataService: DataService ) { }

  @Action(Actions.GetList)
  actionsGetList(ctx: StateContext<IModelState>) {

    return this._dataService.getDataList_DocCashOrder().pipe(
      tap( (list) => { 
        ctx.patchState( {items: list} );
      } )
    );
  }//

  @Selector()
  static selectorGetList(state: IModelState) {
    return new DocCashOrder.List( state.items );
  }//

  @Action(Actions.GetDoc)
  actionsGetDoc(ctx: StateContext<IModelState>, {id}: Actions.GetDoc) {

    if (!id) return;
    return this._dataService.getData_DocCashOrder(id).pipe(
      tap( (val) => { 
        const { cash } = ctx.getState();
        const newCash = cash.filter(item => !(item && item.id === id) );
        newCash.push(val);
        ctx.patchState( {cash: newCash} );
      } )
    );
  }//

  static selectorGetDoc(id: Meta.dbRef, isCopy?: boolean ) {
    return createSelector([DocCashOrderState], (state: IModelState) => {
      const rec = state.cash.find( item=>item.id===id );
      return new DocCashOrder.Doc( rec, isCopy );
    });
  }//

  @Action(Actions.NewDoc)
  actionsNewDoc(ctx: StateContext<IModelState>) {
  }//

  @Action(Actions.SetDeleteMark)
  actionsSetDeleteMark(ctx: StateContext<IModelState>, {id}: Actions.SetDeleteMark) {
  }//

  @Action(Actions.SaveDoc)
  actionsSaveDoc(ctx: StateContext<IModelState>, {obj}: Actions.SaveDoc) {
    return this._dataService.save_DocCashOrder(obj);
  }//


}//

