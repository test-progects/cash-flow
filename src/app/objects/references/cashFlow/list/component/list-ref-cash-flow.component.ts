import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RefCashFlow } from "../../models/ref-cash-flow.metadata";
import { styleKit, getRefIcon, getRefIconStyle } from "@shared/other.util";

@Component({
  selector: 'app-list-ref-cash-flow',
  templateUrl: './list-ref-cash-flow.component.html',
  styleUrls: ['./list-ref-cash-flow.component.scss']
})
export class ListRefCashFlowComponent implements OnInit {

  @Input() list: RefCashFlow.Ref[] = [];
  displayedColumns: string[] = ['icon', 'code', 'name', 'comment'];

  public currentRow: RefCashFlow.Ref;
  @Output() currentRowChange = new EventEmitter<RefCashFlow.Ref>();
  @Output() selectRow = new EventEmitter<void>();

  constructor() { 
  }//

  ngOnInit() {
  }//
  
  keyDown(event){
    const key = event.key;

    switch (key) {
      case "ArrowUp": this.rowUp();
        break;
      case "ArrowDown": this.rowDown();
        break;
      case "Enter": this.selectRow.emit(); 
        break;  
    
      default:
        break;
    }
    
  }//


  rowDown(){
    event.preventDefault();
    if (this.list.length == 0) return;
    if (!this.currentRow) this.currentRow = this.list[0];
    let i = this.list.indexOf(this.currentRow);
    if (i < this.list.length - 1) this.currentRow = this.list[i+1];
    this.currentRowChange.emit(this.currentRow);
  }//

  rowUp() {
    event.preventDefault();
    if (this.list.length == 0) return;
    if (!this.currentRow) this.currentRow = this.list[0];
    let i = this.list.indexOf(this.currentRow);
    if (i > 0) this.currentRow = this.list[i-1];
    this.currentRowChange.emit(this.currentRow);
  }//


  rowClick(row){
    this.currentRow = row;
    this.currentRowChange.emit(this.currentRow);
  }//

  rowDoubleClick(row: RefCashFlow.Ref){
    this.selectRow.emit();
  }//

  getRowStyle(row: RefCashFlow.Ref) {
    return (row === this.currentRow) ? styleKit.currentRow : "";
  }//

  getIcon(row: RefCashFlow.Ref) {
    return getRefIcon(row);
  }//

  getIconStyle(row: RefCashFlow.Ref) {
    return getRefIconStyle(row);
  }//

}//
