import { Component, Input, Output, EventEmitter } from '@angular/core';
//import { MetadataObject } from "../../../models/metadata";

@Component({
  selector: 'app-ref-list-command-panel-up',
  templateUrl: './ref-list-command-panel-up.component.html'
})
export class RefListCommandPanelUpComponent {

  @Input() object: any;

  @Input() options: { select?: boolean };

  @Output("clickMenu") eventClickMenu = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  //

  clickNew() {
    this.eventClickMenu.emit( {name:"clickNew"} );
  }//

  clickCopy() {
    this.eventClickMenu.emit( {name:"clickCopy"} );
  }//

  clickSelect() {
    this.eventClickMenu.emit( {name:"clickSelect"} );
  }//

}//
