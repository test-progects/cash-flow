import { Component, OnInit } from '@angular/core';

import * as Meta from "@objects/models/object.metadata";
import { DocCashOrder } from "../models/doc-cash-order.metadata";

import { Store } from '@ngxs/store';
import { DocCashOrderState as State } from '../store/doc-cash-order.state';
import { ActionsDocCashOrder as Actions } from "../store/doc-cash-order.actions";
import { DataService } from "@services/data.service";

import { Observable } from 'rxjs';
import { delayWhen, tap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  templateUrl: './list-doc-cash-order.container.html',
  styleUrls: ['./list-doc-cash-order.container.scss']
})
export class ListDocCashOrderContainer implements OnInit {

  public object$: Observable<DocCashOrder.List>;
  public currentRow: DocCashOrder.RowList;
  public withChildren: boolean;

  constructor( 
    private readonly store: Store, 
    private _dataService: DataService,  
    private router: Router,
    private route: ActivatedRoute ) { 

      route.data.subscribe(data=>{ this.withChildren = !!data.withChildren });
  }//

  currentRowChange(val) {
    if (!val) return;
    this.currentRow = val;
  }//

  ngOnInit() {

    this.store.dispatch(new Actions.GetList());

    this.object$ = this.store.select(State.selectorGetList)
    .pipe(
      delayWhen(val=>val && val.refresh(this._dataService))
    );

  }//


  getTitle (ref: Meta.dbRef): Observable<string> {
    return this._dataService.getTitle(ref); 
  }//

  clickMenuUp(event) {
    switch (event.name) {
      case "clickNew":
        this.router.navigate(['/ListDocCashOrder', 'DocCashOrder', ""]);
        break;

      case "clickCopy":
        this.newCopy();
        break;
        
      default:
        break;
    }
  }//

  selectRow() {
    if (!this.currentRow) return;
    this.router.navigate(['/ListDocCashOrder', 'DocCashOrder', this.currentRow.id]);
  }//

  newCopy() {
    if (!this.currentRow) return;
    this.router.navigate(['/ListDocCashOrder', 'DocCashOrder', this.currentRow.id, "copy"]);
  }//

}//
