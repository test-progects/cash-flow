import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'cash-flow';

  constructor(  private router: Router ) { }//


  ngOnInit() {}

  clickListDocCashOrder() { 
    this.router.navigate(['/ListDocCashOrder']);
  }//

  clickListRefCashFlow() {
    this.router.navigate(['/ListRefCashFlow']);
  }//

}//



