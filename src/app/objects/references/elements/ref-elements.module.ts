import { NgModule } from '@angular/core';
import { SharedModule } from "@app/modules/shared.module";
//
import { RefCommandPanelUpComponent } from "./ref-command-panel-up/ref-command-panel-up.component";
import { RefCommandPanelDownComponent } from "./ref-command-panel-down/ref-command-panel-down.component";
import { RefListCommandPanelUpComponent } from "./ref-list-command-panel-up/ref-list-command-panel-up.component";

@NgModule({

  imports: [
    SharedModule
  ],

  declarations: [
    RefCommandPanelUpComponent,
    RefCommandPanelDownComponent,
    RefListCommandPanelUpComponent,
  ],

  exports: [
    RefCommandPanelUpComponent,
    RefCommandPanelDownComponent,
    RefListCommandPanelUpComponent,
  ]
})
export class RefElementsModule {}
