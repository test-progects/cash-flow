import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MetadataObject } from "../../../models/object.metadata";

@Component({
  selector: 'app-ref-command-panel-up',
  templateUrl: './ref-command-panel-up.component.html'
})
export class RefCommandPanelUpComponent {

  @Input() object: MetadataObject;

  @Output("clickMenu") eventClickMenu = new EventEmitter<any>();

  constructor() { }

  //

  clickOK() {
    this.eventClickMenu.emit( {name:"clickOK"} );
  }//

  clickSave() {
    this.eventClickMenu.emit( {name:"clickSave"} );
  }//

  clickCopy() {
    this.eventClickMenu.emit( {name:"clickCopy"} );
  }//

  clickDel() {
    this.eventClickMenu.emit( {name:"clickDel"} );
  }//

  clickViewInList() {
    this.eventClickMenu.emit( {name:"clickViewList"} );
  }//

  clickHelp() {
    this.eventClickMenu.emit( {name:"clickHelp"} );
  }//

  clickExit() {
    this.eventClickMenu.emit( {name:"clickExit"} );
  }//

}//

