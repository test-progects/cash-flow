import { Component, OnInit } from '@angular/core';

import { Store } from '@ngxs/store';
import { ActionsDocCashOrder as Actions} from "../store/doc-cash-order.actions";
import { DocCashOrderState as State } from '../store/doc-cash-order.state';
import { DataService } from "@services/data.service";

import { delayWhen } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

import { SelectRefCashFlowContainer } from "@objects/references/cashFlow/listForSelect/select-ref-cash-flow.container";
import {MatDialog} from '@angular/material/dialog';

import * as Meta from "@objects/models/object.metadata";
import { DocCashOrder } from "../models/doc-cash-order.metadata";


@Component({
  templateUrl: './doc-cash-order.container.html',
  styleUrls: ['./doc-cash-order.container.scss']
})
export class DocCashOrderContainer implements OnInit {

  public object: DocCashOrder.Doc;
  private _id: string;
  private _isCopy: boolean;

  constructor( 
    private store: Store, 
    private _dataService: DataService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog
    ) {
      route.params.subscribe((par) => {
        this._id = par['id'];
        this._isCopy = !!par.copy;
        this.loadData();
      });
    }//

  ngOnInit() {}

  loadData() {
    const id = this._id as Meta.dbRef;
    this.store.dispatch(new Actions.GetDoc(id)).toPromise().then(_=>{
      
      this.store.selectOnce(State.selectorGetDoc(id, this._isCopy))
      .pipe(
        delayWhen(val=>{return val.refresh(this._dataService)})
      ).toPromise().then(val=>{
        this.object = val;
      });

    });
  }//
  
  eventFormChange(ctx) {
    if (!event) return;

    switch (ctx.name) {
      case 'clickCashFlow':
        this.onClickCashFlow();
        break;

      default: break;
    }
  }//

  //

  clickMenuUp(event) {
    if (!event) return;
    switch (event.name) {

      case 'clickSave':
        this.onSave();
        break;
    
      default: break;
    }
  }//

  clickMenuDown(event) {

    if (!event) return;

    switch (event.name) {
      case 'clickExit':
        if (this.onClose()) this.router.navigate(['/ListDocCashOrder']);
        break;

      case 'clickOK':
        this.onSave();
        if (this.onClose()) this.router.navigate(['/ListDocCashOrder']);
        break;

      default: break;
    }
  }//

  //

  onClickCashFlow() {
    const dial = this.dialog.open(SelectRefCashFlowContainer, {
      width: '700px',
      height: '80%',
      data: { select: true }
    });

    dial.afterClosed().subscribe(result => {
      if (!result) return;
      this.object.cashFlowItem = result.id;
      this.object.cashFlowItem_title_ = result.name;
      this.refreshObject();
    });

  }//

  onClose() {
    return true;
  }//

  onSave() {
    const record = this.object.getRecordDB();
    this.store.dispatch(new Actions.SaveDoc(record));
  }//

  refreshObject() {
    const tmp = this.object;
    this.object = null;
    setTimeout(() => {
      this.object = tmp;
    }, 0);
  }//



}//
