import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DataService } from "./services/data.service";
import { ProgectModule } from "./modules/progect.module";
import { SharedModule } from "./modules/shared.module";
//import { BreadCrumbsComponent } from './bread-crumbs/bread-crumbs.component';


@NgModule({
  declarations: [
    AppComponent,
    //BreadCrumbsComponent
  ],

  imports: [
    SharedModule,
    ProgectModule,
  ],
  providers: [
    DataService,
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
